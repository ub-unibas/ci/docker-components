# GitLab CI/CD Components for Building Docker Images

[CI/CD components](https://docs.gitlab.com/ee/ci/components/) are reusable
single pipeline configuration units. This repository contains such units for
building Docker images. They are used in projects maintained by the IT
department of the [University Library of Basel](https://ub.unibas.ch).

## Components

### test-image (not yet ready for production!)

This component checks if the base image used in Dockerfile has known vulnerabilities. For this purpose, it uses GitLab's [container-scanning](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning) utility. Supported are the most common distributions (see [list](https://docs.gitlab.com/ee/user/application_security/container_scanning/#supported-distributions)). For more information on container scanning, see the [documentation on GitLab.org](https://docs.gitlab.com/ee/user/application_security/container_scanning). Runs only on tagged commits or commits made on the default branch.

#### Artifacts

TODO

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/docker-components/test-image@main
    inputs:
      secure_log_level: debug
```

#### Inputs

| Input | Description | Value type | Default value |
| ----- | ----------- | ---------- | ------------- |
| `dockerfile` | Path to Dockerfile | `string` | `"${CI_PROJECT_DIR}/Dockerfile"` |
| `secure_log_level` | Log level | `string` | `"info"` |
| `stage` | Job stage | `string` | `"publish"` |


### publish-image

This component creates Docker images and publishes ("pushes") them to the project's [container registry](https://gitlab.switch.ch/help/user/packages/container_registry/index). Depending on the kind of commit and git branch, the Docker image is tagged differently. The following rules apply with decreasing priority.

- If commit tagged according to pattern `/^v?\d+\.\d+\.\d+$/` => Image tag = git tag
- If pushed to default branch => Image tag = `latest`
- If pushed to other branch => Image tag = branch name

#### Example

```yaml
include:
  - component: $CI_SERVER_FQDN/ub-unibas/ci/docker-components/publish-image@main
    inputs:
      dockerfile: ${CI_PROJECT_DIR}/Dockerfile
      stage: test
```

#### Inputs

| Input | Description | Value type | Default value |
| ----- | ----------- | ---------- | ------------- |
| `dockerfile` | Path to Dockerfile | `string` | `"${CI_PROJECT_DIR}/Dockerfile"` |
| `stage` | Job stage | `string` | `"publish"` |
